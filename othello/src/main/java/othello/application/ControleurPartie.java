/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.application;

import othello.partie.Partie;
import othello.partie.joueurs.Humain;
import othello.partie.joueurs.Joueur;
import othello.partie.joueurs.JoueurFactory;
import othello.partie.joueurs.ia.Avance;
import othello.partie.joueurs.ia.Debutant;
import othello.partie.sauvegarde.Sauvegarde;

public class ControleurPartie {

    public void initialiser(int typeJoueur) {
        Partie.getInstance().ajouterJoueur(new Humain());
        Partie.getInstance().ajouterJoueur(JoueurFactory.obtenirJoueur(typeJoueur));
    }

    public void demarrer() {
        Partie.getInstance().demarrerPartie();
    }

    public void sauvegarder(String cheminFichier) {
        Sauvegarde sauvegarde = new Sauvegarde(Partie.getInstance().getJoueurs(),
                Partie.getInstance().getListeCoup(), cheminFichier);
        sauvegarde.sauvegarder();
    }

    public void charger(String cheminFichier) {
        Sauvegarde sauvegarde = new Sauvegarde(cheminFichier);
        sauvegarde.charger();
        Partie.getInstance().setJoueurs(sauvegarde.getJoueurs());
        Partie.getInstance().chargerListeCoup(sauvegarde.getCoupsJoues());
    }

    public void envoyerCoup(int i, int j) {
        Partie.getInstance().jouerCoup(i, j);
    }

    public void visualiser() {
        Partie.getInstance().visualiser();
    }

    public void viderListeCoup() {
        Partie.getInstance().viderListeCoup();
    }
}