package com.uqam.inf5153.tp3.othello.presentation;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class PanneauSauvegarder extends JFileChooser {
    public PanneauSauvegarder() {
        addChoosableFileFilter(new FileNameExtensionFilter("XML (*).xml", "xml"));
        setFileFilter(getChoosableFileFilters()[1]);
        setAcceptAllFileFilterUsed(false);
        showSaveDialog(this);
    }

    public String getCheminFichier() {
        if (getSelectedFile() != null) {
            if (getSelectedFile().toString().contains("xml")) {
                return getSelectedFile().toString();
            } else {
                return getSelectedFile().toString() + ".xml";
            }
        }
        return null;
    }
}