/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.presentation;

import javax.swing.*;
import java.awt.*;

/**
 * @author Guillaume
 */
public class PanneauIA extends PanneauBackground {

    public PanneauIA() {
        setLayout(null);
        JButton btnDifficile = new BoutonDebut("Difficile", 400, 50);
        JButton btnFacile = new BoutonDebut("Facile", 100, 50);
        JLabel lblChoix = new JLabel("Choisissez la difficulté de votre adversaire!");
        lblChoix.setForeground(Color.WHITE);
        Dimension dim = lblChoix.getPreferredSize();
        lblChoix.setBounds(190, 20, dim.width, dim.height);
        add(lblChoix);
        add(btnDifficile);
        add(btnFacile);

        btnDifficile.addActionListener(e -> ControleurUI.getInstance().demarrerPartie(2));
        btnFacile.addActionListener(e -> ControleurUI.getInstance().demarrerPartie(1));
    }
}
