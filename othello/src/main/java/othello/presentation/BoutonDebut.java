/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.presentation;

import javax.swing.*;
import java.awt.*;

/**
 * @author gcoll_000
 */
public class BoutonDebut extends JButton {

    public BoutonDebut(String text, int x, int y) {
        super(text);
        setFont(new Font("Arial", Font.PLAIN, 30));
        setOpaque(false);
        setContentAreaFilled(false);
        Dimension dim = this.getPreferredSize();
        setBounds(x, y, dim.width, dim.height);
        setForeground(Color.WHITE);
        setFocusPainted(false);
    }
}
