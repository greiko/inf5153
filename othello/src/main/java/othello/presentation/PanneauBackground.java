/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.presentation;

import javax.swing.*;
import java.awt.*;

/**
 * @author gcoll_000
 */
public abstract class PanneauBackground extends JPanel {

    @Override
    public void paintComponent(Graphics g) {
        Image img = new ImageIcon("src/image/othello.png").getImage();
        g.drawImage(img, 0, 0, null);
    }
}
