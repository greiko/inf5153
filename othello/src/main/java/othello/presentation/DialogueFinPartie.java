package othello.presentation;

import javax.swing.*;

/**
 * Created by nic-lovin on 7/12/16.
 */
public class DialogueFinPartie extends JOptionPane {

    private int reponse;
    public static final String RECOMMENCER = "Recommencer";
    public static final String QUITTER = "Quitter";
    public static final String VISUALISER = "Visualiser";

    private Object[] options = {RECOMMENCER, QUITTER, VISUALISER};

    protected DialogueFinPartie(String vict) {
        reponse = showOptionDialog(null,
                vict + "\nQue désirez-vous faire?",
                "Partie terminée",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[2]);
    }

    protected String getReponse() {
        if (reponse < 0) {
            return null;
        }
        return options[reponse].toString();
    }
}
