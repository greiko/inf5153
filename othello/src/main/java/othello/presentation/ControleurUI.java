package othello.presentation;

import othello.application.ControleurPartie;
import othello.partie.Partie;
import othello.partie.plateau.Couleur;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ControleurUI implements Observer {

    private FrameBase frameBase;
    private static final ControleurUI controleurUI = new ControleurUI();
    private ControleurPartie controleurPartie;

    private ControleurUI() {
        frameBase = new FrameBase();
    }

    public static ControleurUI getInstance() {
        return controleurUI;
    }

    protected void nouvellePartie() {
        frameBase.nouvellePartie();
    }

    protected void nouveauIA() {
        frameBase.nouveauIA();
    }

    protected void jouerCoup(int i, int j) {
        controleurPartie.envoyerCoup(i, j);
    }

    protected void demarrerPartie(int typeJoueur) {
        frameBase.creationPlateauDeJeu();
        controleurPartie = new ControleurPartie();
        controleurPartie.initialiser(typeJoueur);
        Partie.getInstance().getPlateau().addObserver(this);
        controleurPartie.demarrer();
    }

    protected void chargerPartie(String urlFichier) {
        frameBase.creationPlateauDeJeu();

        controleurPartie = new ControleurPartie();
        controleurPartie.charger(urlFichier);
        Partie.getInstance().getPlateau().addObserver(this);
        controleurPartie.demarrer();
    }

    protected void sauvegarderPartie(String urlFichier) {
        controleurPartie.sauvegarder(urlFichier);
    }

    @Override
    public void update(Observable o, Object arg) {
        Object[] objects = (Object[]) arg;
        Couleur[][] cases = (Couleur[][]) objects[0];
        ArrayList<int[]> coupsPossibles = (ArrayList<int[]>) objects[1];
        boolean fini = (boolean) objects[2];
        frameBase.mettreAJour(cases, coupsPossibles, fini);
    }

    protected void gererFinJeu(String str) {
        switch (str) {
            case DialogueFinPartie.QUITTER:
                System.exit(0);
                break;
            case DialogueFinPartie.RECOMMENCER:
                Partie.getInstance().getPlateau().addObserver(this);
                controleurPartie.viderListeCoup();
                controleurPartie.demarrer();
                break;
            case DialogueFinPartie.VISUALISER:
                Partie.getInstance().getPlateau().addObserver(this);
                controleurPartie.visualiser();
                break;
        }
    }
}
