package othello.presentation;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Created by nic-lovin on 7/9/16.
 */
public class DialogueCharger extends JFileChooser {
    public DialogueCharger() {
        addChoosableFileFilter(new FileNameExtensionFilter("XML (*).xml", "xml"));
        setFileFilter(getChoosableFileFilters()[1]);
        setAcceptAllFileFilterUsed(false);
        showOpenDialog(this);
    }
}
