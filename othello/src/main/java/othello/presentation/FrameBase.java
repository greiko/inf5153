package othello.presentation;

import othello.partie.plateau.Couleur;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Frame de Base, la première qui apparait
 */
public class FrameBase extends JFrame {

    private PanneauDebut panneauDebut;
    private PanneauJeu panneauJeu;

    /**
     * Ajout du panneau Menu
     */
    public FrameBase() {
        setTitle("Othello");
        panneauDebut = new PanneauDebut();
        add(panneauDebut);
        setResizable(false);
        setMinimumSize(new Dimension(670, 430));
        setSize(670, 430);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    protected void nouvellePartie() {
        getContentPane().removeAll();
        add(new PanneauNouvellePartie());
        getContentPane().revalidate();
        repaint();
    }

    protected void nouveauIA() {
        getContentPane().removeAll();
        add(new PanneauIA());
        getContentPane().revalidate();
        repaint();
    }

    protected void creationPlateauDeJeu() {
        getContentPane().removeAll();
        add(panneauJeu = new PanneauJeu());
        getContentPane().revalidate();
        repaint();
    }

    protected void mettreAJour(Couleur[][] cases, ArrayList<int[]> coupsPossibles, boolean fini) {
        panneauJeu.mettreAJourJeu(cases, coupsPossibles, fini);
        getContentPane().revalidate();
    }
}
