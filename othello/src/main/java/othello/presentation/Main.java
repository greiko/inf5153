package othello.presentation;


public class Main {

    /**
     * Début du programme
     *
     * @param args
     */
    public static void main(String[] args) {
        ControleurUI.getInstance();
    }
}
