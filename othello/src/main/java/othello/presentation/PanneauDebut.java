/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.presentation;

import javax.swing.*;

/**
 * @author gcoll_000
 */
public class PanneauDebut extends PanneauBackground {

    public PanneauDebut() {
        setLayout(null);
        JButton btnNouvelle = new BoutonDebut("Nouvelle partie", 50, 50);
        JButton btnCharger = new BoutonDebut("Charger une partie", 350, 50);
        add(btnCharger);
        add(btnNouvelle);

        btnCharger.addActionListener(e -> {
            DialogueCharger pc = new DialogueCharger();
            if (pc.getSelectedFile() != null) {
                ControleurUI.getInstance().chargerPartie(pc.getSelectedFile().toString());
            }
        });
        btnNouvelle.addActionListener(e -> ControleurUI.getInstance().nouvellePartie());
    }
}
