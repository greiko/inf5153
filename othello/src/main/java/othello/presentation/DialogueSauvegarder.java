package othello.presentation;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class DialogueSauvegarder extends JFileChooser {
    public DialogueSauvegarder() {
        addChoosableFileFilter(new FileNameExtensionFilter("XML (*).xml", "xml"));
        setFileFilter(getChoosableFileFilters()[1]);
        setAcceptAllFileFilterUsed(false);
        showSaveDialog(this);
    }

    protected String getCheminFichier() {
        if (getSelectedFile() != null) {
            if (getSelectedFile().toString().contains("xml")) {
                return getSelectedFile().toString();
            } else {
                return getSelectedFile().toString() + ".xml";
            }
        }
        return null;
    }
}