/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.presentation;

import javax.swing.*;
import java.awt.*;

/**
 * @author Guillaume
 */
public class PanneauNouvellePartie extends PanneauBackground {

    public PanneauNouvellePartie() {
        setLayout(null);
        JButton btnHumain = new BoutonDebut("Humain", 50, 50);
        JButton btnAI = new BoutonDebut("Intelligence Artificielle", 300, 50);
        JLabel lblChoix = new JLabel("Choisissez votre adversaire!");
        lblChoix.setForeground(Color.WHITE);
        Dimension dim = lblChoix.getPreferredSize();
        lblChoix.setBounds(250, 20, dim.width, dim.height);
        add(lblChoix);
        add(btnHumain);
        add(btnAI);

        btnAI.addActionListener(e -> ControleurUI.getInstance().nouveauIA());
        btnHumain.addActionListener(e -> {
            JOptionPane.showMessageDialog(null, "Cette fonctionnalité n'est pas implémenté", "Error", JOptionPane.ERROR_MESSAGE);
        });
    }
}
