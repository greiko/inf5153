/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.presentation;

import othello.partie.plateau.Couleur;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class PanneauJeu extends JPanel {

    private final int SIZE = 8;
    private static int joueur = 0;
    private final JButton[][] buttons;
    private JLabel nbrNoir;
    private JLabel nbrBlanc;
    private final String blancStr = "pions blancs: ";
    private final String noirStr = "pions noirs: ";

    public PanneauJeu() {
        setLayout(new BorderLayout());
        JPanel grille = new JPanel(new GridLayout(SIZE, SIZE));
        JPanel j1 = new JPanel();
        JPanel j2 = new JPanel();
        j1.add(new JLabel("Joueur 1"));
        JButton btnj1 = new JButton();
        btnj1.setEnabled(false);
        btnj1.setBackground(Color.WHITE);
        JButton btnj2 = new JButton();
        btnj2.setEnabled(false);
        btnj2.setBackground(Color.BLACK);
        j1.setPreferredSize(new Dimension(120, 200));
        j2.setPreferredSize(new Dimension(120, 200));
        j1.add(btnj1);
        j2.add(btnj2);
        j2.add(new JLabel("Joueur 2"));
        j1.add(nbrBlanc = new JLabel(blancStr));
        j2.add(nbrNoir = new JLabel(noirStr));
        buttons = new JButton[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                buttons[i][j] = new JButton();
                grille.add(buttons[i][j]);
                buttons[i][j].addActionListener(new MatrixButtonListener(i, j));
            }
        }

        add(grille, BorderLayout.CENTER);
        add(j1, BorderLayout.WEST);
        add(j2, BorderLayout.EAST);
        JButton sauvegarder = new JButton("Sauvegarder");
        sauvegarder.addActionListener(e -> {
            DialogueSauvegarder ps = new DialogueSauvegarder();
            if (ps.getCheminFichier() != null) {
                JOptionPane.showMessageDialog(null, "Votre partie a bien été sauvegardé", "Sauvegarde", JOptionPane.INFORMATION_MESSAGE);
                ControleurUI.getInstance().sauvegarderPartie(ps.getCheminFichier());
            }
        });
        this.add(sauvegarder, BorderLayout.SOUTH);
    }

    protected void mettreAJourJeu(Couleur[][] cases, ArrayList<int[]> coupsPossibles, boolean fini) {
        int noir = 0;
        int blanc = 0;
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                buttons[i][j].setEnabled(false);
                buttons[i][j].setBackground(new Color(0, 102, 0));
                if (cases[i][j] == Couleur.BLANC) {
                    buttons[i][j].setBackground(Color.WHITE);
                    buttons[i][j].setEnabled(false);
                    blanc++;
                }
                if (cases[i][j] == Couleur.NOIR) {
                    buttons[i][j].setBackground(Color.BLACK);
                    buttons[i][j].setEnabled(false);
                    noir++;
                }
            }
        }
        if (fini) {
            String vict;
            if (noir > blanc) {
                vict = "Le joueur noir a gagné " + noir + " à " + blanc + "!";
            } else if (noir < blanc) {
                vict = "Le joueur blanc a gagné " + blanc + " à " + noir + "!";
            } else {
                vict = "Match nul!";
            }

            gererFinJeu(vict);
            return;
        }

        if (joueur >= 0 && joueur % 2 == 0) {
            if (coupsPossibles.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Jouez n'avez pas de coup à jouer");
            }
            for (int[] coup : coupsPossibles) {
                buttons[coup[0]][coup[1]].setEnabled(true);
                buttons[coup[0]][coup[1]].setBackground(Color.ORANGE);
            }
        }
        joueur++;
        nbrBlanc.setText(blancStr + blanc);
        nbrNoir.setText(noirStr + noir);
        revalidate();
    }

    private void gererFinJeu(String vict) {
        joueur = 0;

        DialogueFinPartie df = new DialogueFinPartie(vict);
        if (df.getReponse() == null) {
            return;
        } else if (df.getReponse().equals(DialogueFinPartie.VISUALISER)) {
            joueur = -100;
        }
        ControleurUI.getInstance().gererFinJeu(df.getReponse());
    }

    private class MatrixButtonListener implements ActionListener {

        private int i;
        private int j;

        public MatrixButtonListener(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ControleurUI.getInstance().jouerCoup(i, j);
        }
    }
}
