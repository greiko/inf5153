package com.uqam.inf5153.tp3.othello.presentation;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Created by nic-lovin on 7/9/16.
 */
public class PanneauCharger extends JFileChooser {
    public PanneauCharger() {
        addChoosableFileFilter(new FileNameExtensionFilter("XML (*).xml", "xml"));
        setFileFilter(getChoosableFileFilters()[1]);
        setAcceptAllFileFilterUsed(false);
        showOpenDialog(this);
    }
}
