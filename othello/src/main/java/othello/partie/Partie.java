package othello.partie;

import othello.partie.joueurs.Joueur;
import othello.partie.plateau.Plateau;

/**
 * Created by nic-lovin on 6/1/16.
 */
public class Partie {

    private static Partie instance = new Partie();
    private ListeJoueur joueurs;
    private Plateau plateau;
    private ListeCoup listeCoup;
    private boolean aJoue = false;
    private int positionX = -1;
    private int positionY = -1;

    public static Partie getInstance() {
        return instance;
    }

    private Partie() {
        joueurs = new ListeJoueur();
        plateau = new Plateau();
        listeCoup = new ListeCoup();
    }

    public void ajouterJoueur(Joueur joueur) {
        joueurs.ajouterJoueur(joueur);
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public ListeJoueur getJoueurs() {
        return joueurs;
    }

    public void setJoueurs(ListeJoueur joueurs) {
        this.joueurs = joueurs;
    }

    public ListeCoup getListeCoup() {
        return listeCoup;
    }

    public void setListeCoup(ListeCoup listeCoup) {
        this.listeCoup = listeCoup;
    }

    public void viderListeCoup() {
        this.listeCoup.vider();
    }

    public void demarrerPartie() {
        plateau.notifier(false);
        Thread partieThread;
        partieThread = new Thread() {
            @Override
            public void run() {
                Coup coup;
                while (!estFinie()) {
                    try {
                        while (!plateau.getCoupsDisponibles().estVide() && !aJoue) {
                            Thread.sleep(100);
                        }
                        coup = joueurs.getJoueur(0).jouerCoup(plateau, positionX, positionY);
                        plateau.actualiser(coup);
                        listeCoup.ajouterCoup(coup);
                        aJoue = false;
                        if (!estFinie()) {
                            Thread.sleep(1000);
                            coup = joueurs.getJoueur(1).jouerCoup(plateau);
                            plateau.actualiser(coup);
                            listeCoup.ajouterCoup(coup);
                        }
                    } catch (InterruptedException e) {
                    }
                }
                viderPartie();
            }
        };
        partieThread.start();
    }

    public void visualiser() {
        plateau.notifier(false);
        Thread visualierThread = new Thread() {
            @Override
            public void run() {
                while (!estFinie()) {
                    try {
                        for (Coup coup : listeCoup.getListeCoups()) {
                            Thread.sleep(1000);
                            plateau.actualiser(coup);
                        }
                    } catch (InterruptedException e) {
                    }
                }
                viderPartie();
            }
        };
        visualierThread.start();
    }

    public void jouerCoup(int positionX, int positionY) {
        aJoue = true;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    private boolean estFinie() {
        return plateau.estFinie();
    }

    public void chargerListeCoup(ListeCoup listeCoup) {
        setListeCoup(listeCoup);
        for (Coup coup : listeCoup.getListeCoups()) {
            plateau.actualiser(coup);
        }
    }

    private void viderPartie() {
        Plateau plateau = this.plateau;
        this.plateau = new Plateau();
        aJoue = false;
        positionX = -1;
        positionY = -1;
        plateau.notifier(true);
    }
}
