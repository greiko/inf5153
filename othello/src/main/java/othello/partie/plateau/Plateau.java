package othello.partie.plateau;

import othello.partie.Coup;
import othello.partie.ListeCoup;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by nic-lovin on 6/1/16.
 */
public class Plateau extends Observable {

    private final int LARGEUR = 8;
    private final int HAUTEUR = 8;
    private Couleur[][] plateauJeu;
    private Couleur dernierCoup = Couleur.NOIR;
    private ListeCoup listeCoup;

    public Plateau() {
        plateauJeu = new Couleur[HAUTEUR][LARGEUR];
        listeCoup = new ListeCoup();
        remplirPlateau();
        initialiserPlateau();
        calculerCoupsDisponibles();
    }

    public Plateau(Plateau plateau) {
        plateauJeu = new Couleur[HAUTEUR][LARGEUR];

        for (int i = 0; i < HAUTEUR; i++) {
            for (int j = 0; j < LARGEUR; j++) {
                plateauJeu[i][j] = plateau.getPlateauJeu()[i][j];
            }
        }

        listeCoup = new ListeCoup();
        for (int i = 0; i < plateau.getCoupsDisponibles().getTaille(); i++) {
            listeCoup.ajouterCoup(plateau.getCoupsDisponibles().getCoup(i));
        }
    }

    private void remplirPlateau() {
        for (int i = 0; i < HAUTEUR; i++) {
            for (int j = 0; j < LARGEUR; j++) {
                plateauJeu[i][j] = Couleur.VIDE;
            }
        }
    }

    private void initialiserPlateau() {
        plateauJeu[3][3] = Couleur.BLANC;
        plateauJeu[4][4] = Couleur.BLANC;
        plateauJeu[3][4] = Couleur.NOIR;
        plateauJeu[4][3] = Couleur.NOIR;
    }

    public void actualiser(Coup coup) {
        dernierCoup = autreCouleur(dernierCoup);
        if (coup == null) {
            calculerCoupsDisponibles();
            notifier(false);
            return;
        }
        int x = coup.getX();
        int y = coup.getY();
        Couleur couleur = coup.getCouleur();
        plateauJeu[x][y] = couleur;
        calculerCasesAdjacentes(x, y, couleur, true);
        calculerCoupsDisponibles();
        notifier(false);
    }

    private boolean calculerCasesAdjacentes(int x, int y, Couleur couleur, boolean mettreAJour) {
        boolean jouable = false;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (j < 0 || j >= HAUTEUR || i < 0 || i >= HAUTEUR) {
                    continue;
                }
                if (plateauJeu[i][j] == autreCouleur(couleur)) {
                    int incX = 0;
                    int incY = 0;
                    if (i < x) {
                        incX = -1;
                    } else if (i > x) {
                        incX = 1;
                    }
                    if (j < y) {
                        incY = -1;
                    } else if (j > y) {
                        incY = 1;
                    }
                    if (changerCouleur(i, j, incX, incY, couleur, mettreAJour)) {
                        jouable = true;
                    }
                }
            }
        }
        return jouable;
    }

    private boolean changerCouleur(int i, int j, int incX, int incY, Couleur couleur, boolean mettreAJour) {
        if (i < 0 || j < 0 || i > HAUTEUR - 1 || j > HAUTEUR - 1) {
            return false;
        }
        if (plateauJeu[i][j] == couleur) {
            return true;
        } else if (plateauJeu[i][j] == Couleur.VIDE) {
            return false;
        }

        if (changerCouleur(i + incX, j + incY, incX, incY, couleur, mettreAJour)) {
            if (mettreAJour) {
                plateauJeu[i][j] = couleur;
            }
            return true;
        }
        return false;
    }

    public ListeCoup getCoupsDisponibles() {
        return listeCoup;
    }

    private void calculerCoupsDisponibles() {
        listeCoup.vider();
        for (int i = 0; i < HAUTEUR; i++) {
            for (int j = 0; j < LARGEUR; j++) {
                if (plateauJeu[i][j] == Couleur.VIDE) {
                    if (calculerCasesAdjacentes(i, j, autreCouleur(dernierCoup), false)) {
                        listeCoup.ajouterCoup(new Coup(autreCouleur(dernierCoup), i, j));
                    }
                }
            }
        }
    }

    private Couleur[][] getPlateauJeu() {
        return plateauJeu;
    }

    private Couleur autreCouleur(Couleur couleur) {
        if (couleur == Couleur.BLANC) {
            return Couleur.NOIR;
        } else if (couleur == Couleur.NOIR) {
            return Couleur.BLANC;
        }
        return null;
    }

    public boolean estFinie() {
        return nombrePionsBlancs() == 0 || nombrePionsNoirs() == 0 || nombreCasesVides() == 0;
    }

    public int nombrePionsNoirs() {
        return nombreCases(Couleur.NOIR);
    }

    private int nombrePionsBlancs() {
        return nombreCases(Couleur.BLANC);
    }

    private int nombreCasesVides() {
        return nombreCases(Couleur.VIDE);
    }

    public void notifier(boolean fini) {
        setChanged();
        ArrayList<int[]> listeCoups = new ArrayList<>();
        for (int i = 0; i < this.listeCoup.getTaille(); i++) {
            int[] coupInt = {this.listeCoup.getCoup(i).getX(),
                    this.listeCoup.getCoup(i).getY()};
            listeCoups.add(coupInt);
        }
        Object[] listeObject = {plateauJeu, listeCoups, fini};
        notifyObservers(listeObject);
    }

    private int nombreCases(Couleur couleur) {
        int nbCase = 0;
        for (int i = 0; i < HAUTEUR; i++) {
            for (int j = 0; j < LARGEUR; j++) {
                if (plateauJeu[i][j] == couleur) {
                    nbCase++;
                }
            }
        }
        return nbCase;
    }
}
