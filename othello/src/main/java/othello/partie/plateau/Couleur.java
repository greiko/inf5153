package othello.partie.plateau;

/**
 * Created by nic-lovin on 6/1/16.
 */
public enum Couleur {
    BLANC, NOIR, VIDE
}
