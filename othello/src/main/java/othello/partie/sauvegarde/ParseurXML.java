package othello.partie.sauvegarde;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;

class ParseurXML {


    //TODO REFACTOR?
    static void sauvegarderXML(Sauvegarde sauvegarde) {

        String typeJoueurUn = InformationPartie.determinerTypeJoueur(sauvegarde.getJoueurs().getJoueur(0));
        String typeJoueurDeux = InformationPartie.determinerTypeJoueur(sauvegarde.getJoueurs().getJoueur(1));
        String difficulteJoueurUn = InformationPartie.determinerDifficulte(sauvegarde.getJoueurs().getJoueur(0));
        String difficulteJoueurDeux = InformationPartie.determinerDifficulte(sauvegarde.getJoueurs().getJoueur(1));

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element racine = document.createElement("othello");
            Element joueurs = document.createElement("joueurs");
            Element joueurUnXML = document.createElement("joueur");
            Element joueurDeuxXML = document.createElement("joueur");
            Element typeUn = document.createElement("type");
            Element typeDeux = document.createElement("type");
            Element difficulteUn = document.createElement("difficulte");
            Element difficulteDeux = document.createElement("difficulte");

            //==============================================================================

            typeUn.appendChild(document.createTextNode(typeJoueurUn));
            difficulteUn.appendChild(document.createTextNode(difficulteJoueurUn));
            joueurUnXML.appendChild(typeUn);
            if (typeJoueurUn.equalsIgnoreCase("IA")) {
                joueurUnXML.appendChild(difficulteUn);
            }


            typeDeux.appendChild(document.createTextNode(typeJoueurDeux));
            difficulteDeux.appendChild(document.createTextNode(difficulteJoueurDeux));
            joueurDeuxXML.appendChild(typeDeux);
            if (typeJoueurDeux.equalsIgnoreCase("IA")) {
                joueurDeuxXML.appendChild(difficulteDeux);
            }

            document.appendChild(racine);
            racine.appendChild(joueurs);
            joueurs.appendChild(joueurUnXML);
            joueurs.appendChild(joueurDeuxXML);


            //========================================================================================

            Element listeCoups = document.createElement("listeCoups");

            ArrayList<String> listeCaseX = InformationPartie.coupCaseX(sauvegarde);
            ArrayList<String> listeCaseY = InformationPartie.coupCaseY(sauvegarde);
            ArrayList<String> listeCaseCouleur = InformationPartie.coupCaseCouleur(sauvegarde);

            for (int i = 0; i < listeCaseX.size(); i++) {
                Element coupXML = document.createElement("coup");
                Element caseX = document.createElement("caseX");
                Element caseY = document.createElement("caseY");
                Element couleur = document.createElement("couleur");
                caseX.appendChild(document.createTextNode(listeCaseX.get(i)));
                caseY.appendChild(document.createTextNode(listeCaseY.get(i)));
                couleur.appendChild(document.createTextNode(listeCaseCouleur.get(i)));
                coupXML.appendChild(caseX);
                coupXML.appendChild(caseY);
                coupXML.appendChild(couleur);
                listeCoups.appendChild(coupXML);
            }
            if (!listeCaseX.isEmpty()) {
                racine.appendChild(listeCoups);
            }

            //=======================================================================================


            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(sauvegarde.getCheminFichier()));
            transformer.transform(source, result);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    static void chargerXML(Sauvegarde sauvegarde) {
        try {
            File fichierXML = new File(sauvegarde.getCheminFichier());
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(fichierXML);
            document.getDocumentElement().normalize();

            //========================================================================================

            NodeList nodeList = document.getElementsByTagName("othello");
            Element element = (Element) nodeList.item(0);
            String difficulteJ1 = "";
            String difficulteJ2 = "";
            String j1 = element.getElementsByTagName("type").item(0).getTextContent();
            String j2 = element.getElementsByTagName("type").item(1).getTextContent();

            if (j1.equalsIgnoreCase("IA")) {
                difficulteJ1 = element.getElementsByTagName("difficulte").item(0).getTextContent();
            }
            if (j2.equalsIgnoreCase("IA")) {
                if (!difficulteJ1.equalsIgnoreCase("")) {
                    difficulteJ2 = element.getElementsByTagName("difficulte").item(1).getTextContent();
                } else {
                    difficulteJ2 = element.getElementsByTagName("difficulte").item(0).getTextContent();
                }
            }

            //========================================================================================

            sauvegarde.setJoueurs(InformationPartie.listeJoueurBuilder(j1, j2, difficulteJ1, difficulteJ2));
            for (int i = 0; element.getElementsByTagName("caseX").item(i) != null; i++) {
                String caseX = element.getElementsByTagName("caseX").item(i).getTextContent();
                String caseY = element.getElementsByTagName("caseY").item(i).getTextContent();
                String couleur = element.getElementsByTagName("couleur").item(i).getTextContent();
                InformationPartie.coupBuilder(sauvegarde, caseX, caseY, couleur);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
