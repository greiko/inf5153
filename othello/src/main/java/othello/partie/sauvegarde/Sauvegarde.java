package othello.partie.sauvegarde;

import othello.partie.ListeCoup;
import othello.partie.ListeJoueur;



/*
Dans le UML, listeCoups et listeJoueurs netait pas import...
 */

public class Sauvegarde {

    private ListeJoueur joueurs;
    private ListeCoup coupsJoues;
    private String cheminFichier;

    public Sauvegarde(ListeJoueur joueurs, ListeCoup coupsJoues, String cheminFichier) {
        this.joueurs = joueurs;
        this.coupsJoues = coupsJoues;
        this.cheminFichier = cheminFichier;
    }

    public Sauvegarde(String cheminFichier) {
        this.cheminFichier = cheminFichier;
    }


    public void sauvegarder() {
        ParseurXML.sauvegarderXML(this);
    }

    public void charger() {

        ParseurXML.chargerXML(this);
    }

    /*
    GETTER AND SETTERS
     */
    public ListeJoueur getJoueurs() {
        return joueurs;
    }

    public void setJoueurs(ListeJoueur joueurs) {
        this.joueurs = joueurs;
    }

    public ListeCoup getCoupsJoues() {
        return coupsJoues;
    }

    public void setCoupsJoues(ListeCoup coupsJoues) {
        this.coupsJoues = coupsJoues;
    }

    public String getCheminFichier() {
        return cheminFichier;
    }

}

