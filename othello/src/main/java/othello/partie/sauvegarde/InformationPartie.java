package othello.partie.sauvegarde;

import othello.partie.Coup;
import othello.partie.ListeCoup;
import othello.partie.ListeJoueur;
import othello.partie.joueurs.Humain;
import othello.partie.joueurs.Joueur;
import othello.partie.joueurs.ia.Avance;
import othello.partie.joueurs.ia.Debutant;
import othello.partie.plateau.Couleur;

import java.util.ArrayList;

class InformationPartie {

    static String determinerTypeJoueur(Joueur joueur) {
        if (joueur.getClass() == Humain.class) {
            return "HUMAIN";
        }
        return "IA";
    }

    static String determinerDifficulte(Joueur joueur) {
        if (joueur.getClass() == Debutant.class) {
            return "DEBUTANT";
        }
        if (joueur.getClass() == Avance.class) {
            return "AVANCE";
        }
        return "";
    }

    static Joueur definirTypeJoueur(String type, String difficulte) {
        if (type.equalsIgnoreCase(Humain.getTypeNom())) {
            return new Humain();
        }
        if (difficulte.equalsIgnoreCase(Debutant.getTypeNom())) {
            return new Debutant();
        }
        if (difficulte.equalsIgnoreCase(Avance.getTypeNom())) {
            return new Avance();
        }
        return null;
    }


    static ArrayList<String> coupCaseX(Sauvegarde sauvegarde) {
        ArrayList<String> listeCaseX = new ArrayList<>();
        for (Coup coup : sauvegarde.getCoupsJoues().getListeCoups()) {
            listeCaseX.add(Integer.toString(coup.getX()));
        }
        return listeCaseX;
    }

    static ArrayList<String> coupCaseY(Sauvegarde sauvegarde) {
        ArrayList<String> listeCaseY = new ArrayList<>();
        for (Coup coup : sauvegarde.getCoupsJoues().getListeCoups()) {
            listeCaseY.add(Integer.toString(coup.getY()));
        }
        return listeCaseY;
    }

    static ArrayList<String> coupCaseCouleur(Sauvegarde sauvegarde) {
        ArrayList<String> listeCaseCouleur = new ArrayList<>();
        for (Coup coup : sauvegarde.getCoupsJoues().getListeCoups()) {
            listeCaseCouleur.add(coup.getCouleur().name());
        }
        return listeCaseCouleur;
    }

    static ListeJoueur listeJoueurBuilder(String typeJoueurUn, String typeJoueurDeux, String difficulteJ1, String difficulteJ2) {
        ListeJoueur listeJoueur = new ListeJoueur();
        listeJoueur.ajouterJoueur(InformationPartie.definirTypeJoueur(typeJoueurUn, difficulteJ1));
        listeJoueur.ajouterJoueur(InformationPartie.definirTypeJoueur(typeJoueurDeux, difficulteJ2));
        return listeJoueur;
    }

    static void coupBuilder(Sauvegarde sauvegarde, String caseX, String caseY, String couleur) {
        if (sauvegarde.getCoupsJoues() == null) {
            sauvegarde.setCoupsJoues(new ListeCoup());
        }
        Coup coup = new Coup(Couleur.valueOf(couleur), Integer.parseInt(caseX), Integer.parseInt(caseY));
        sauvegarde.getCoupsJoues().ajouterCoup(coup);
    }

}
