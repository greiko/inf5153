package othello.partie;

import othello.partie.plateau.Couleur;

/**
 * Created by nic-lovin on 6/1/16.
 */
public class Coup {

    private Couleur couleur;
    private int x;
    private int y;

    public Coup(Couleur couleur, int x, int y) {
        this.couleur = couleur;
        this.x = x;
        this.y = y;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Coup{" + "couleur=" + couleur + ", x=" + x + ", y=" + y + '}';
    }
}
