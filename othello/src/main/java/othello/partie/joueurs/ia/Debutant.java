package othello.partie.joueurs.ia;

import othello.partie.Coup;
import othello.partie.joueurs.IA;
import othello.partie.plateau.Plateau;

import java.util.Random;

/**
 * Created by nic-lovin on 6/1/16.
 */
public class Debutant extends IA {
    @Override
    public Coup jouerCoup(Plateau plateau) {
        if (plateau.getCoupsDisponibles().getTaille() > 0) {
            int index = new Random().nextInt(plateau.getCoupsDisponibles().getTaille());
            return plateau.getCoupsDisponibles().getCoup(index);
        }
        return null;
    }

    public static String getTypeNom() {
        return "Debutant";
    }
}
