package othello.partie.joueurs.ia;

import othello.partie.Coup;
import othello.partie.ListeCoup;
import othello.partie.joueurs.IA;
import othello.partie.plateau.Plateau;

import java.util.PriorityQueue;

/**
 * Created by nic-lovin on 6/1/16.
 */
public class Avance extends IA {

    public static String getTypeNom() {
        return "Avance";
    }

    @Override
    public Coup jouerCoup(Plateau plateau) {
        ListeCoup listeCoup = plateau.getCoupsDisponibles();
        if (listeCoup.getTaille() > 0) {
            PriorityQueue<Pair> pQueue = new PriorityQueue<>();
            for (int i = 0; i < listeCoup.getTaille(); i++) {
                Coup coup = listeCoup.getCoup(i);
                Plateau temp = new Plateau(plateau);
                temp.actualiser(coup);
                pQueue.add(new Pair(temp.nombrePionsNoirs(), coup));  //todo: verif de la couleur du AI?
            }
            return pQueue.poll().coup;
        } else {
            return null;
        }
    }

    private class Pair implements Comparable<Pair> {

        public final int score;
        public final Coup coup;

        public Pair(int s, Coup c) {
            score = s;
            coup = c;
        }

        @Override
        public int compareTo(Pair autre) {
            return Integer.compare(autre.score, this.score);
        }

        @Override
        public String toString() {
            return "score: " + this.score + " coup: " + coup.toString() + "\n";
        }
    }

}
