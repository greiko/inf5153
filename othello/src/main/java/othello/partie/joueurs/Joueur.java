package othello.partie.joueurs;

import othello.partie.Coup;
import othello.partie.plateau.Plateau;

/**
 * Created by nic-lovin on 6/1/16.
 */
public abstract class Joueur {

    public Coup jouerCoup(Plateau plateau) {
        return null;
    }

    public Coup jouerCoup(Plateau plateau, int x, int y) {
        return null;
    }

}
