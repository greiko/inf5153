package othello.partie.joueurs;

import othello.partie.Coup;
import othello.partie.plateau.Plateau;

/**
 * Created by nic-lovin on 6/1/16.
 */
public class Humain extends Joueur {

    public static String getTypeNom() {
        return "Humain";
    }

    @Override
    public Coup jouerCoup(Plateau plateau, int x, int y) {
        for (int i = 0; i < plateau.getCoupsDisponibles().getTaille(); i++) {
            if (plateau.getCoupsDisponibles().getCoup(i).getX() == x
                    && plateau.getCoupsDisponibles().getCoup(i).getY() == y) {
                return plateau.getCoupsDisponibles().getCoup(i);
            }
        }
        return null;
    }
}
