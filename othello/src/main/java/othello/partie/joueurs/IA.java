package othello.partie.joueurs;

import othello.partie.Coup;
import othello.partie.plateau.Plateau;

public abstract class IA extends Joueur {

    @Override
    public abstract Coup jouerCoup(Plateau plateau);
}
