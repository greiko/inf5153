package othello.partie.joueurs;


import othello.partie.joueurs.ia.Avance;
import othello.partie.joueurs.ia.Debutant;

public class JoueurFactory {

    public static Joueur obtenirJoueur(int typeJoueur) {
        switch (typeJoueur) {
            case 0:
                return new Humain();
            case 1:
                return new Debutant();
            case 2:
                return new Avance();
        }
        return null;
    }
}
