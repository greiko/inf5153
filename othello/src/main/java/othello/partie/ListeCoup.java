package othello.partie;

import java.util.ArrayList;

/**
 * Created by nic-lovin on 6/8/16.
 */
public class ListeCoup {

    private ArrayList<Coup> listeCoups;

    public ListeCoup() {
        listeCoups = new ArrayList<>();
    }

    public void ajouterCoup(Coup coup) {
        listeCoups.add(coup);
    }

    public void vider() {
        listeCoups.clear();
    }

    public int getTaille() {
        return listeCoups.size();
    }

    public boolean estVide() {
        return listeCoups.isEmpty();
    }

    /*
    test
     */
    public Coup getCoup(int index) {
        return listeCoups.get(index);
    }

    public ArrayList<Coup> getListeCoups() {
        return listeCoups;
    }

    @Override
    public String toString() {
        return "ListeCoup{" + "listeCoups=" + listeCoups + '}';
    }
}
