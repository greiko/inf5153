package othello.partie;

import othello.partie.joueurs.Joueur;

import java.util.ArrayList;

/**
 * Created by nic-lovin on 6/1/16.
 */
public class ListeJoueur {

    private ArrayList<Joueur> joueurs;

    public ListeJoueur() {
        joueurs = new ArrayList<>();
    }

    public void ajouterJoueur(Joueur joueur) {
        this.joueurs.add(joueur);
    }

    public ArrayList<Joueur> getJoueurs() {
        return this.joueurs;
    }

    public Joueur getJoueur(int i) {
        return this.joueurs.get(i);
    }

}
